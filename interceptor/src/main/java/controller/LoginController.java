package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    @RequestMapping("/login")
    public String login(HttpSession session, String username, String userpass) {
        session.setAttribute("userLoginInfo", username);
        return "main";
    }

    @RequestMapping("/gologin")
    public String gologin() {
        return "login";
    }

    @RequestMapping("/main")
    public String main() {
        return "main";
    }
}
