package controller;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pojo.User;
import util.JSONUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
//@RestController
public class UserController {
    @RequestMapping("/j1")
//    @RequestMapping(value = "/j1",produces = "application/json;charset=utf-8")
    @ResponseBody//不走视图解析器，直接返回字符串
    public String json1() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        User user = new User("张三", 3, "男");
        return mapper.writeValueAsString(user);
    }

    @RequestMapping("/j2")
    @ResponseBody
    public String json2() {
        List<User> userList = new ArrayList<>();
        User user1 = new User("张三", 3, "男");
        User user2 = new User("李四", 4, "男");
        User user3 = new User("小明", 5, "男");
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
//        return mapper.writeValueAsString(userList);
        return JSONUtils.getJson(userList);
    }

    @RequestMapping("/j3")
    @ResponseBody
    public String json3() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Date date = new Date();//默认格式为Timestamp
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return mapper.writeValueAsString(sdf.format(date));
    }

    @RequestMapping("/j4")
    @ResponseBody
    public String json4() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();//默认格式为Timestamp
        mapper.setDateFormat(sdf);
        return mapper.writeValueAsString(sdf.format(date));
    }

    @RequestMapping("/j5")
    @ResponseBody
    public String json5() {
        Date date = new Date();//默认格式为Timestamp
        return JSONUtils.getJson(date,"yyyy-MM-dd HH:mm:ss");
    }

    @RequestMapping("/j6")
    @ResponseBody
    public String json6() {
        Date date = new Date();//默认格式为Timestamp
        return JSONUtils.getJson(date);
    }

    @RequestMapping("/j7")
    @ResponseBody
    public String json7(){
        List<User> userList = new ArrayList<>();
        User user1 = new User("张三", 3, "男");
        User user2 = new User("李四", 4, "男");
        User user3 = new User("小明", 5, "男");
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        return JSON.toJSONString(userList);
    }
}
