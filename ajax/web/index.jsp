<%--
  Created by IntelliJ IDEA.
  User: Dell
  Date: 2021/12/31
  Time: 10:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>$Title$</title>
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
          crossorigin="anonymous"></script>
  <script>
    function a1() {
      $.post({
        url: "${pageContext.request.contextPath}/a1",
        data: {'name': $("#txtName").val()},
        success: function (data, status) {
          alert(data);
          alert(status);
        }
      })
    }
  </script>
</head>
<body>

<%--onblur：失去焦点触发事件--%>
<label for="txtName">用户名:</label><input type="text" id="txtName" onblur="a1()"/>

</body>
</html>
